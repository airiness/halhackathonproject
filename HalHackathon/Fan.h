//=====================================//
//Author: Josh Chen                    //
//Date:   2019/07/23                   //
//Fan.h								   //
//=====================================//
#pragma once
#include "AnimatedTexture.h"
#include "Player.h"
#include "pch.h"
#include "ItemManager.h"
using DirectX::SimpleMath::Vector2;

class Fan {

public:
	Fan(ID3D11Device1* device, DirectX::AudioEngine* audio);
	~Fan();
	void SetScreenSize(UINT x, UINT y);
	void Update(float elapsedTime, DirectX::Keyboard * keyboard, Player & player, ItemManager & itemManager);
	void Draw(DirectX::SpriteBatch* spriteBatch);
	Vector2& GetFanPosition();
	void GetBatteryResource();
	void Reset();
private:
	UINT m_screensizeX;
	UINT m_screensizeY;

	bool switchIsOn;
	int m_fanWidth;
	int m_fanHeight;
	float batteryFull;
	float batteryLeft;
	float batteryConsumeSpeed;
	float moveDistance;
	float fanPower;

	Vector2 m_fanPos;
	Vector2 m_fanOrigin;

	//Fan Textures
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_fanIdleTexture;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_fanSpinTexture;
	std::unique_ptr<AnimatedTexture> m_fanAnimateTexture;

	//Battery Textures
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_battery100Texture;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_battery75Texture;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_battery50Texture;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_battery25Texture;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_battery0Texture;
	int m_BTRWidth;
	int m_BTRHeight;
	Vector2 m_BTRPos;
	Vector2 m_BTROrigin;

	//Switch Textures
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_switchOnTexture;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_switchOffTexture;

	//Sound Effects
	std::unique_ptr<DirectX::SoundEffect> m_fanOperatingSE;

	//KeyBord State Tracker
	std::unique_ptr<DirectX::Keyboard::KeyboardStateTracker> m_keyboardStateTracker;

};
