//=====================================//
//Author: Josh Chen                    //
//Date:   2019/07/22                   //
//Player.h                             //
//=====================================//
#include "Player.h"

using namespace DirectX;
using Microsoft::WRL::ComPtr;

Player::Player() {

}

Player::~Player() {

}

void Player::Initialize(ID3D11Device1* device) {

	Microsoft::WRL::ComPtr<ID3D11Resource> resource;
	//
	DX::ThrowIfFailed(CreateWICTextureFromFile(device, L"Assets/Textures/plane00.png", resource.GetAddressOf(), m_playerIdleTexture.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(CreateWICTextureFromFile(device, L"Assets/Textures/plane01.png", resource.GetAddressOf(), m_playerJumpTexture.ReleaseAndGetAddressOf()));

	ComPtr<ID3D11Texture2D> player;
	DX::ThrowIfFailed(resource.As(&player));
	CD3D11_TEXTURE2D_DESC playerDesc;
	player->GetDesc(&playerDesc);

	m_playerWidth = playerDesc.Width;
	m_playerHeight = playerDesc.Height;

	m_playerOrigin.x = float(m_playerWidth / 2);
	m_playerOrigin.y = float(m_playerHeight / 2);

	m_playerPos = { 100.f, 200.f };
	m_playerPosOld = m_playerPos;

	m_playerMove = { 0.f, -5.f };

	isWalk = true;
	isRun = false;
	isFan = false;

	walkSpeed = 3.0f;

	runSpeed = 4.0f;
	gravity = 0.6f;

	m_playerTransformState = TRANSFORM_STATE_NORMAL;
}

void Player::SetScreenSize(UINT x, UINT y)
{
	m_screensizeX = x;
	m_screensizeY = y;
}

void Player::Update(float elapsedTime, DirectX::Keyboard * keyboard) {

	m_playerPosOld = m_playerPos;

	auto kb = keyboard->GetState();

	/*if (kb.W)
	{
		isFan = true;
	}
	else
	{
		isFan = false;
	}*/

	if (kb.X)
	{
		isRun = true;
		isWalk = false;
	}
	else
	{
		isRun = false;
		isWalk = true;
	}

	//Move Right
	if (isWalk)
		m_playerMove.x = walkSpeed;
	if (isRun)
		m_playerMove.x = runSpeed;

	Gravity();

	/*if (isFan)
	{
		Fan();
	}*/

	if (m_playerMove.y > 5.f)
	{
		m_playerMove.y = 5.f;
	}
	else if (m_playerMove.y < -8.f)
	{
		m_playerMove.y = -8.f;
	}

	if (isWalk)
	{
		if (m_playerPos.x < m_screensizeX / 3.f)
		{
			m_playerPos.x += m_playerMove.x;
		}
		else if (m_playerPos.x > m_screensizeX / 3.f + 2.f)
		{
			m_playerPos.x -= 2.f;
		}
	}
	if (isRun)
	{
		m_playerPos.x += m_playerMove.x - walkSpeed;
	}
	m_playerPos.y += m_playerMove.y;


	if (m_playerPos.x < m_playerWidth / 2.f)
	{
		m_playerPos.x = m_playerWidth / 2.f;
	}

	if (m_playerPos.y < 10)
	{
		m_playerPos.y = 10;
	}
	if (m_playerPos.y > m_screensizeY + m_playerHeight)
	{
		//m_playerPos.y = m_screensizeY + m_playerHeight;
	}
}

void Player::Draw(DirectX::SpriteBatch* spriteBatch)
{
	if (m_playerMove.y >= 0)
	{
		if (m_playerTransformState == TRANSFORM_STATE_NORMAL)
		{
			spriteBatch->Draw(m_playerIdleTexture.Get(), m_playerPos, nullptr, Colors::White, 0.f, m_playerOrigin, 1.f);
		}
		else if (m_playerTransformState == TRANSFORM_STATE_01)
		{

		}
		else if (m_playerTransformState == TRANSFORM_STATE_02)
		{

		}
		else if (m_playerTransformState == TRANSFORM_STATE_03)
		{

		}
	}
	else if (m_playerMove.y < 0)
	{
		if (m_playerTransformState == TRANSFORM_STATE_NORMAL)
		{
			spriteBatch->Draw(m_playerJumpTexture.Get(), m_playerPos, nullptr, Colors::White, 0.f, m_playerOrigin, 1.f);
		}
		else if (m_playerTransformState == TRANSFORM_STATE_01)
		{

		}
		else if (m_playerTransformState == TRANSFORM_STATE_02)
		{

		}
		else if (m_playerTransformState == TRANSFORM_STATE_03)
		{

		}
	}
}

void Player::Gravity()
{
	m_playerMove.y += gravity;
}

void Player::Fan(float fanPower)
{
	m_playerMove.y -= fanPower;
}

bool Player::GetIsRun()
{
	return isRun;
}

Vector2 & Player::GetPlayerPosition()
{
	return m_playerPos;
}

TRANSFORM_STATE Player::GetPlayerTransformState()
{
	return m_playerTransformState;
}

void Player::Reset()
{
	m_playerPos = { 100.f, 200.f };
	m_playerPosOld = m_playerPos;

	m_playerMove = { 0.f, -5.f };

	isWalk = true;
	isRun = false;
	isFan = false;

	walkSpeed = 3.0f;

	runSpeed = 4.0f;
	gravity = 0.6f;

	m_playerTransformState = TRANSFORM_STATE_NORMAL;
}

