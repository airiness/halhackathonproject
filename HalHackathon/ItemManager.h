//=====================================//
//Author: Josh Chen                    //
//Date:   2019/07/23                   //
//Item.h                               //
//=====================================//
#pragma once

#include "AnimatedTexture.h"
#include "pch.h"
using DirectX::SimpleMath::Vector2;

enum ItemType {
	batteryRecover = 0,
	playerTransform,
};

typedef struct Item
{
	ItemType m_type;
	bool isUse;
	Vector2 m_itemPos;
	Vector2 m_itemOrigin;
	int m_itemWidth;
	int m_itemHeight;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> itemTexture;
};

class ItemManager {

public:
	ItemManager();
	ItemManager(ID3D11Device1* device);
	~ItemManager();
	void Update(float elapsedTime, DirectX::Keyboard * keyboard);
	void Draw(DirectX::SpriteBatch* spriteBatch);
	void createItem();
	std::vector<Item> items;

private:
	
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> itemType1Texture;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> itemType2Texture;
	//std::vector<Item> items;
	float createDelay;
};
