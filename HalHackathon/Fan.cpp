//=====================================//
//Author: Josh Chen                    //
//Date:   2019/07/23                   //
//Fan.cpp                              //
//=====================================//
#include "Fan.h"
using namespace DirectX;
using Microsoft::WRL::ComPtr;

Fan::Fan(ID3D11Device1* device, DirectX::AudioEngine* audio) {
	switchIsOn = true;
	fanPower = 0.5f;
	batteryFull = 750.f;
	batteryLeft = batteryFull;
	batteryConsumeSpeed = 2.f;

	Microsoft::WRL::ComPtr<ID3D11Resource> resource;
	DX::ThrowIfFailed(CreateWICTextureFromFile(device, L"Assets/Textures/FanIdle.png", resource.GetAddressOf(), m_fanIdleTexture.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(CreateWICTextureFromFile(device, L"Assets/Textures/FanSpin.png", nullptr, m_fanSpinTexture.ReleaseAndGetAddressOf()));

	ComPtr<ID3D11Texture2D> fan;
	DX::ThrowIfFailed(resource.As(&fan));
	CD3D11_TEXTURE2D_DESC fanDesc;
	fan->GetDesc(&fanDesc);

	m_fanWidth = fanDesc.Width;
	m_fanHeight = fanDesc.Height;

	m_fanOrigin.x = float(m_fanWidth / 2.f);
	m_fanOrigin.y = float(m_fanHeight);

	m_fanAnimateTexture = std::make_unique<AnimatedTexture>(m_fanOrigin, 1.f);
	m_fanAnimateTexture->Load(m_fanSpinTexture.Get(), 3, 20);

	m_fanPos.x = 100;
	m_fanPos.y = float(m_screensizeY);

	moveDistance = 5.0f;

	m_keyboardStateTracker = std::make_unique<DirectX::Keyboard::KeyboardStateTracker>();

	//Battery Texture
	DX::ThrowIfFailed(CreateWICTextureFromFile(device, L"Assets/Textures/BTR100.png", resource.GetAddressOf(), m_battery100Texture.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(CreateWICTextureFromFile(device, L"Assets/Textures/BTR75.png", resource.GetAddressOf(), m_battery75Texture.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(CreateWICTextureFromFile(device, L"Assets/Textures/BTR50.png", resource.GetAddressOf(), m_battery50Texture.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(CreateWICTextureFromFile(device, L"Assets/Textures/BTR25.png", resource.GetAddressOf(), m_battery25Texture.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(CreateWICTextureFromFile(device, L"Assets/Textures/BTR0.png", resource.GetAddressOf(), m_battery0Texture.ReleaseAndGetAddressOf()));
	ComPtr<ID3D11Texture2D> BTR;
	DX::ThrowIfFailed(resource.As(&BTR));
	CD3D11_TEXTURE2D_DESC BTRDesc;
	BTR->GetDesc(&BTRDesc);

	/*m_BTRWidth = BTRDesc.Width;
	m_BTRHeight = BTRDesc.Height;*/

	m_BTROrigin.x = float(BTRDesc.Width / 2.f);
	m_BTROrigin.y = float(BTRDesc.Height);

	//Sound
	m_fanOperatingSE = std::make_unique<SoundEffect>(audio, L"Assets/Sounds/Fan_cut1.wav");
}

Fan::~Fan() {

}

void Fan::SetScreenSize(UINT x, UINT y)
{
	m_screensizeX = x;
	m_screensizeY = y;
}

void Fan::Update(float elapsedTime, DirectX::Keyboard * keyboard, Player & player, ItemManager & itemManager) {
	//Get player position
	Vector2 playerPos;
	playerPos = player.GetPlayerPosition();

	//KeyBoard Event
	auto kb = keyboard->GetState();
	m_keyboardStateTracker->Update(kb);

	if (kb.Right) {			//move fan right
		m_fanPos.x += moveDistance;
	}
	if (kb.Left) {			//move fan left
		m_fanPos.x -= moveDistance;
	}

	if (kb.S) {			//fan's switch
		switchIsOn = true;

		//Play sound
		m_fanOperatingSE->Play();
	}				
	else {
		switchIsOn = false;
		
	}

	//Check if switch is on or off
	if (switchIsOn && batteryLeft > 0.f) {

		//lose battery
		batteryLeft -= batteryConsumeSpeed;

		//Blowing wind, if player is above, it will be blew up
		if ((playerPos.x >= m_fanPos.x - m_fanWidth / 2.f) && (playerPos.x <= m_fanPos.x + m_fanWidth / 2.f) && (playerPos.y < m_fanPos.y - m_fanHeight) ) {
			//According to the y distance between fan and player, give player different fan power
			float yDistanceProportiom = (1.f / (m_fanPos.y - playerPos.y)) * 50.f;
			//player.Fan(fanPower * yDistanceProportiom);
			player.Fan(1.f);
		}
	}


	//Check battery left
	if (batteryLeft <= 0) {
		switchIsOn = false;
	}

	//Fan's left and right moving limit
	if (m_fanPos.x + m_fanWidth / 2.f > m_screensizeX)
	{
		m_fanPos.x = m_screensizeX - m_fanWidth / 2.f;
	}
	if (m_fanPos.x < m_fanWidth / 2.f)
	{
		m_fanPos.x = m_fanWidth / 2.f;
	}

	//Check if touch a item
	for (auto && item : itemManager.items) {
		if ( abs(m_fanPos.x - item.m_itemPos.x) < m_fanWidth / 2.f ) {
			item.isUse = false;
			GetBatteryResource();
		}
	}

	//Update Animate
	if (switchIsOn) {
		m_fanAnimateTexture->Update(elapsedTime);
	}
}

void Fan::Draw(DirectX::SpriteBatch* spriteBatch) {
	if (!switchIsOn) {
		spriteBatch->Draw(m_fanIdleTexture.Get(), { m_fanPos.x,float(m_screensizeY) }, nullptr, Colors::White, 0.f, m_fanOrigin, .5f);
	}
	else {
		m_fanAnimateTexture->Draw(spriteBatch, { m_fanPos.x,float(m_screensizeY) }, .5f);
	}

	//Check battery left
	int batteryPercent = (batteryLeft / batteryFull) * 100.f;
	if (batteryPercent > 75) {
		//4�i
		spriteBatch->Draw(m_battery100Texture.Get(), { m_fanPos.x,float(m_screensizeY) }, nullptr, Colors::White, 0.f, m_BTROrigin, 0.5f);
	}
	else if (batteryPercent > 50 && batteryPercent <= 75) {
		//3�i
		spriteBatch->Draw(m_battery75Texture.Get(), { m_fanPos.x,float(m_screensizeY) }, nullptr, Colors::White, 0.f, m_BTROrigin, 0.5f);
	}
	else if (batteryPercent > 25 && batteryPercent <= 50) {
		//2�i
		spriteBatch->Draw(m_battery50Texture.Get(), { m_fanPos.x,float(m_screensizeY) }, nullptr, Colors::White, 0.f, m_BTROrigin, 0.5f);
	}
	else if (batteryPercent > 0 && batteryPercent <= 25) {
		//1�i
		spriteBatch->Draw(m_battery25Texture.Get(), { m_fanPos.x,float(m_screensizeY) }, nullptr, Colors::White, 0.f, m_BTROrigin, 0.5f);
	}
	else if (batteryPercent == 0) {
		//0�i
		spriteBatch->Draw(m_battery0Texture.Get(), { m_fanPos.x,float(m_screensizeY) }, nullptr, Colors::White, 0.f, m_BTROrigin, 0.5f);

	}
}

Vector2& Fan::GetFanPosition() {
	return m_fanPos;
}

void Fan::GetBatteryResource() {
	batteryLeft += 200.0f;
	if (batteryLeft > batteryFull)
		batteryLeft = batteryFull;
}

void Fan::Reset()
{
	switchIsOn = true;
	fanPower = 0.5f;
	batteryFull = 1000000.f;
	batteryLeft = batteryFull;
	batteryConsumeSpeed = 0.1f;
	m_fanPos.x = 100;
	m_fanPos.y = 700;
	moveDistance = 5.0f;
	m_BTRPos.x = 256.f;
	m_BTRPos.y = 824.f;
}