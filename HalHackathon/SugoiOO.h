//2019.7.24, refactor and fix some bugs--zei
#pragma once
#include "ScrollingBackground.h"
#include "Player.h"
#include "Fan.h"
#include "ItemManager.h"

class SugoiOO
{
private:
	enum SUGOI_STATE
	{
		NONE,
		TITLE,
		INGAME,
		RESULT
	};
	SUGOI_STATE m_gameState;

	enum FADE_STATE
	{
		FADE_STATE_NONE,
		FADE_STATE_FADEIN,
		FADE_STATE_WAIT,
		FADE_STATE_FADEOUT,
	};
	FADE_STATE m_fadeState;
	int waitTime;
public:
	SugoiOO();
	~SugoiOO();

	void Initialize(ID3D11Device1* device, DirectX::AudioEngine* audio);
	void InitializeRelateWindowSize(UINT width, UINT height);
	void Update(float deltaTime, DirectX::Keyboard* keyboard, DirectX::Mouse* mouse, DirectX::GamePad* gamepad);
	void Draw(DirectX::SpriteBatch* spriteBatch);
	void Shutdown();
	void SetFade(float time, SUGOI_STATE nextSceen);
private:
	//KeyboardStateTracker
	std::unique_ptr<DirectX::Keyboard::KeyboardStateTracker> m_keyboardStateTracker;

	//Font
	std::unique_ptr<DirectX::SpriteFont> m_font;
	DirectX::SimpleMath::Vector2 m_fontPos;

	//Texture
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_title_start;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_title_exit;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_result;
	RECT m_fullscreenRect;//FullScreenRect

	//Scrolling Background
	std::unique_ptr<ScrollingBackground> m_ScrollingBG_cloud1;
	std::unique_ptr<ScrollingBackground> m_ScrollingBG_cloud0;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_cloud1;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_cloud0;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_BG;

	

	//Fade Scene change
	int SelectState;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_fade;
	DirectX::SimpleMath::Vector2 m_fadePos;
	DirectX::SimpleMath::Vector2 m_fadeOrigin;
	float fadeTextureWidth;
	float fadeTextureHeight;

	//Screen size
	float m_screenWidth;
	float m_screenHeight;

	//Player
	std::unique_ptr<Player> m_Player;
	//Fan
	std::unique_ptr<Fan> m_Fan;

	//ItemManager
	std::unique_ptr<ItemManager> m_ItemManager;

	//BGM
	std::unique_ptr<DirectX::SoundEffect> m_BGM;
	std::unique_ptr<DirectX::SoundEffectInstance> m_BGMLoop;

	float ResultPoint;
	SUGOI_STATE NextSceen;
};

