//=====================================//
//Author: Josh Chen                    //
//Date:   2019/07/23                   //
//Item.cpp                             //
//=====================================//
#include "ItemManager.h"
#include <vector>
#include <random>

using namespace DirectX;
using Microsoft::WRL::ComPtr;

ItemManager::ItemManager() {

}

ItemManager::ItemManager(ID3D11Device1* device) {
	DX::ThrowIfFailed(CreateWICTextureFromFile(device, L"Assets/Textures/item0.png", nullptr, itemType1Texture.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(CreateWICTextureFromFile(device, L"Assets/Textures/item1.png", nullptr, itemType2Texture.ReleaseAndGetAddressOf()));

	createDelay = 2.f;
}

ItemManager::~ItemManager() {

}

void ItemManager::Update(float elapsedTime, DirectX::Keyboard * keyboard) {
	createDelay -= elapsedTime;
	if (createDelay < 0.f) {
		createItem();

		float randTime = TOOLS::randf(1.f, 10.f);
		createDelay = randTime;
	}

}

void ItemManager::Draw(DirectX::SpriteBatch* spriteBatch) {

	if (items.size() != 0) {
		for (auto item : items) {
			if (item.m_type == batteryRecover) {
				if (item.isUse) {
					spriteBatch->Draw(item.itemTexture.Get(), item.m_itemPos, nullptr, Colors::White, 0.f, item.m_itemOrigin, .25f);
				}
			}
		}
	}
}

void ItemManager::createItem() {

	Item item_New;
	item_New.m_itemWidth = 40;
	item_New.m_itemHeight = 40;
	item_New.m_itemOrigin = { 20.f, 20.f };

	item_New.isUse = true;

	float randNum = TOOLS::randf(1.f, 100.f);
	if (randNum <= 50.f) {
		item_New.m_type = batteryRecover;
		item_New.itemTexture = itemType1Texture;
	}
	else {
		item_New.m_type = playerTransform;
		item_New.itemTexture = itemType2Texture;
	}

	//Set item position
	if (item_New.m_type == batteryRecover) {
		item_New.m_itemPos.y = 950;
		randNum = TOOLS::randf(100.f, 1320.f);
		item_New.m_itemPos.x = randNum;
	}
	else if (item_New.m_type == playerTransform) {
		randNum = TOOLS::randf(100.f, 500.f);
		item_New.m_itemPos.y = randNum;
		randNum = TOOLS::randf(1.f, 100.f);
		item_New.m_itemPos.x = randNum;
	}

	items.push_back(item_New);
}
