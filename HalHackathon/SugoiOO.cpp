#include "pch.h"
#include "SugoiOO.h"

SugoiOO::SugoiOO()
{
}

SugoiOO::~SugoiOO()
{
}

//CreateDevice
void SugoiOO::Initialize(ID3D11Device1 * device, DirectX::AudioEngine * audio)
{
	//game state to title
	m_gameState = TITLE;
	m_fadeState = FADE_STATE_NONE;
	ResultPoint = 0;

	SelectState = 1;

	//KeyboardStateTracker
	m_keyboardStateTracker = std::make_unique<DirectX::Keyboard::KeyboardStateTracker>();

	Microsoft::WRL::ComPtr<ID3D11Resource> resource;
	//Title Texture
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/BG/ittle0.png", nullptr, m_title_start.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/BG/ittle1.png", nullptr, m_title_exit.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/BG/result.png", nullptr, m_result.ReleaseAndGetAddressOf()));
	
	//Fade
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/BG/fade.png", resource.GetAddressOf(), m_fade.ReleaseAndGetAddressOf()));
	Microsoft::WRL::ComPtr<ID3D11Texture2D> fade;
	DX::ThrowIfFailed(resource.As(&fade));
	CD3D11_TEXTURE2D_DESC fadeDesc;
	fade->GetDesc(&fadeDesc);
	m_fadeOrigin.x = float(fadeDesc.Width / 2);
	m_fadeOrigin.y = float(fadeDesc.Height / 2);
	fadeTextureWidth = fadeDesc.Width;
	fadeTextureHeight = fadeDesc.Height;

	//ScrollingBackground
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/BG/forest.png", resource.GetAddressOf(), m_cloud1.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/BG/bg.png", resource.GetAddressOf(), m_BG.ReleaseAndGetAddressOf()));
	DX::ThrowIfFailed(DirectX::CreateWICTextureFromFile(device, L"Assets/Textures/BG/cloud0.png", resource.GetAddressOf(), m_cloud0.ReleaseAndGetAddressOf()));
	m_ScrollingBG_cloud1 = std::make_unique<ScrollingBackground>();
	m_ScrollingBG_cloud0 = std::make_unique<ScrollingBackground>();
	m_ScrollingBG_cloud1->Load(m_cloud1.Get());
	m_ScrollingBG_cloud0->Load(m_cloud0.Get());

	//Player
	m_Player = std::make_unique<Player>();
	m_Player->Initialize(device);

	//Fan
	m_Fan = std::make_unique<Fan>(device, audio);

	//ItemManager
	m_ItemManager = std::make_unique<ItemManager>(device);

	//Font
	m_font = std::make_unique<DirectX::SpriteFont>(device, L"Assets/Fonts/Jokerman32.spritefont");

	//Sound
	m_BGM = std::make_unique<DirectX::SoundEffect>(audio, L"Assets/Sounds/BGM.wav");

	m_BGMLoop = m_BGM->CreateInstance();

	float BGMVolume = .5f;
	
	m_BGMLoop->SetVolume(BGMVolume);
	m_BGMLoop->Play(true);

}

//CreateSource
void SugoiOO::InitializeRelateWindowSize(UINT width, UINT height)
{
	m_Player->SetScreenSize(width, height);
	m_Fan->SetScreenSize(width, height);
	m_screenWidth = width;
	m_screenHeight = height;

	//we will fixed windows size -no!you won't--zei 2019.7.24
	m_fullscreenRect.left = 0;
	m_fullscreenRect.top = 0;
	m_fullscreenRect.right = m_screenWidth;
	m_fullscreenRect.bottom = m_screenHeight;

	m_fadePos.x = m_screenWidth + fadeTextureWidth / 2;
	m_fadePos.y = float(m_screenHeight) / 2.f;

	m_ScrollingBG_cloud1->SetWindow(m_screenWidth, m_screenHeight);
	m_ScrollingBG_cloud0->SetWindow(m_screenWidth, m_screenHeight);

	//Font
	m_fontPos.x = m_screenWidth / 2.f;
	m_fontPos.y = m_screenHeight / 2.f;
}

void SugoiOO::Update(float deltaTime, DirectX::Keyboard * keyboard, DirectX::Mouse * mouse, DirectX::GamePad * gamepad)
{
	auto kb = keyboard->GetState();
	m_keyboardStateTracker->Update(kb);

	if (m_gameState == TITLE)
	{
		if (m_fadeState == FADE_STATE_NONE)
		{
			//title
			if (m_keyboardStateTracker->pressed.Left || m_keyboardStateTracker->pressed.A)
				SelectState--;

			if (m_keyboardStateTracker->pressed.Right || m_keyboardStateTracker->pressed.D)
				SelectState++;

			if (SelectState < 1)
				SelectState = 1;

			if (SelectState > 2)
				SelectState = 2;

			if (m_keyboardStateTracker->pressed.Enter)
			{
				if (SelectState == 1)
				{
					SetFade(deltaTime, INGAME);
				}
				else if (SelectState == 2)
				{
					PostQuitMessage(0);
				}
			}
		}
	}
	else if (m_gameState == INGAME)
	{

		//Update Game
		if (m_fadeState == FADE_STATE_NONE)
		{
			//Update Player
			m_Player->Update(deltaTime, keyboard);


			if (m_Player->GetPlayerPosition().x >= m_screenWidth / 3.f)
			{
				m_ScrollingBG_cloud1->Update(deltaTime * 300);
				m_ScrollingBG_cloud0->Update(deltaTime * 200);
			}

			//Update Fan
			m_Fan->Update(deltaTime, keyboard, *m_Player, *m_ItemManager);

			//Update ItemManager
			m_ItemManager->Update(deltaTime, keyboard);

		

			//Point
			if (m_Player->GetPlayerTransformState() == TRANSFORM_STATE_NORMAL)
			{
				ResultPoint += 0.1f;
			}
			else if (m_Player->GetPlayerTransformState() == TRANSFORM_STATE_01)
			{
				ResultPoint += 5.0f;
			}


			Vector2 m_playerPosition = m_Player->GetPlayerPosition();

			if (m_playerPosition.y > m_screenHeight + 200)
			{
				SetFade(deltaTime, RESULT);
			}
		}
	}
	else if (m_gameState == RESULT)
	{
		if (m_fadeState == FADE_STATE_NONE)
		{

			//Update Result
			if (m_keyboardStateTracker->pressed.Enter)
			{
				m_Player->Reset();
				m_Fan->Reset();
				ResultPoint = 0.0f;
				SetFade(deltaTime, TITLE);
			}
		}
	}

	if (m_fadeState == FADE_STATE_FADEOUT)
	{
		if (m_fadePos.x > float(m_screenWidth) / 2.f)
		{
			m_fadePos.x -= 30;
		}
		else
		{
			m_fadeState = FADE_STATE_WAIT;
		}
	}
	else if (m_fadeState == FADE_STATE_WAIT)
	{
		if (waitTime > 0)
		{
			waitTime--;
		}
		else
		{
			m_gameState = NextSceen;
			m_fadeState = FADE_STATE_FADEIN;
		}
	}
	else if (m_fadeState == FADE_STATE_FADEIN)
	{
		if (m_fadePos.x > -float(fadeTextureWidth) / 2.f)
		{
			m_fadePos.x -= 30;
		}
		else
		{
			//StopBGM();
			m_fadeState = FADE_STATE_NONE;
		}
	}

}


//Render
void SugoiOO::Draw(DirectX::SpriteBatch * spriteBatch)
{

	if (m_gameState == TITLE)
	{
		//
		if (SelectState == 1)
			spriteBatch->Draw(m_title_start.Get(), m_fullscreenRect);
		else if (SelectState == 2)
			spriteBatch->Draw(m_title_exit.Get(), m_fullscreenRect);
	}
	else if (m_gameState == INGAME)
	{
		//Draw Game
		//ScrollingBackground
		spriteBatch->Draw(m_BG.Get(), m_fullscreenRect);
		m_ScrollingBG_cloud1->Draw(spriteBatch);
		m_ScrollingBG_cloud0->Draw(spriteBatch);
		m_Player->Draw(spriteBatch);
		m_Fan->Draw(spriteBatch);
		m_ItemManager->Draw(spriteBatch);
	}
	else if (m_gameState == RESULT)
	{
		spriteBatch->Draw(m_result.Get(), m_fullscreenRect);
		//Draw Result
		std::wstring output1 = std::wstring(L"Distance: ") + std::to_wstring(int(ResultPoint)) + std::wstring(L" cm");
		std::wstring output2 = std::wstring(L"GAME OVER");


		DirectX::SimpleMath::Vector2 posTmp = {1200.f,400.f };
		m_font->DrawString(spriteBatch, output1.c_str(), posTmp, DirectX::Colors::Aqua, 0.f, { 125.f,200.f }, 1.8f);
	}

	if (m_fadeState != FADE_STATE_NONE)
	{
		spriteBatch->Draw(m_fade.Get(), m_fadePos, nullptr, DirectX::Colors::White, 0.f, m_fadeOrigin/*backBufferHeight / fadeTextureHeight*/);
	}
}

void SugoiOO::Shutdown()
{
	m_keyboardStateTracker.reset();
	m_font.reset();
	m_Player.reset();
	m_Fan.reset();
	m_ItemManager.reset();
	m_BGM.reset();
	m_BGMLoop.reset();
	m_ScrollingBG_cloud0.reset();
	m_ScrollingBG_cloud1.reset();
	m_fade.Reset();
	m_title_start.Reset();
	m_title_exit.Reset();
	m_result.Reset();
}

void SugoiOO::SetFade(float time, SUGOI_STATE nextSceen)
{
	if (m_fadeState == FADE_STATE_NONE)
	{
		m_fadeState = FADE_STATE_FADEOUT;
		m_fadePos.x = m_screenWidth + fadeTextureWidth / 2;
	}
	waitTime = time * 60 * 15;
	NextSceen = nextSceen;
}
