//=====================================//
//Author: Josh Chen                    //
//Date:   2019/07/22                   //
//Player.h                             //
//=====================================//
#pragma once

#include "AnimatedTexture.h"
//#include "SugoiOO.h"
#include "pch.h"
using DirectX::SimpleMath::Vector2;

enum TRANSFORM_STATE
{
	TRANSFORM_STATE_NORMAL = 0,
	TRANSFORM_STATE_01,
	TRANSFORM_STATE_02,
	TRANSFORM_STATE_03,
};

class Player {
public:
	Player();
	~Player();
	void Initialize(ID3D11Device1* device);
	void SetScreenSize(UINT x,UINT y);
	void Update(float elapsedTime, DirectX::Keyboard * keyboard);
	void Draw(DirectX::SpriteBatch* spriteBatch);
	void Gravity();
	void Fan(float fanPower);
	bool GetIsRun();
	Vector2& GetPlayerPosition();
	TRANSFORM_STATE GetPlayerTransformState();

	void Reset();
private:
	UINT m_screensizeX;
	UINT m_screensizeY;

	bool isWalk;
	bool isRun;
	bool isFan;
	float walkSpeed;
	float runSpeed;
	float gravity;
	int m_playerWidth;
	int m_playerHeight;
	TRANSFORM_STATE m_playerTransformState;

	DirectX::SimpleMath::Vector2 m_playerPos;
	DirectX::SimpleMath::Vector2 m_playerMove;
	DirectX::SimpleMath::Vector2 m_playerPosOld;


	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_playerIdleTexture;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_playerJumpTexture;
	DirectX::SimpleMath::Vector2 m_playerOrigin;

	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_playerTransform01IdleTexture;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_playerTransform01JumpTexture;
	DirectX::SimpleMath::Vector2 m_playerOrigin01;

	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_playerTransform02IdleTexture;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_playerTransform02JumpTexture;
	DirectX::SimpleMath::Vector2 m_playerOrigin02;

	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_playerTransform03IdleTexture;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_playerTransform03JumpTexture;
	DirectX::SimpleMath::Vector2 m_playerOrigin03;
};