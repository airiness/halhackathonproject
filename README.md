# HalHackathon
A 2D game made in period of Hal Hackathon Day by four people team.<br>
using [DirectXTK](https://github.com/microsoft/DirectXTK "github link").<br>
using git version control:<br>
![](images/hctGit.png)
### Game Screen Shot
![](images/hctTitle.png)
![](images/tobus.gif)
![](images/hctFinal.png)